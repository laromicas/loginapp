<?php

class Helpers {

	public static function object_to_array($obj) {
		if(is_object($obj)) $obj = (array) $obj;
		if(is_array($obj)) {
			$new = array();
			foreach($obj as $key => $val) {
				$new[$key] = Helpers::object_to_array($val);
			}
		} else $new = $obj;
		return $new;		 
	}

	public static function find_many($model, $getArray = false) {
		if(!$getArray) {
			return $model->find_many();
		} else {
			if(is_array($getArray)) {
				$Questions = $model->find_many();
				$result = array();
				foreach ($Questions as $question) {
					$res = array();
					foreach ($getArray as $id) {
						$res[$id] = $question->$id;
					}
					$result[] = $res;
				}
				return $result;
			} elseif(is_object($getArray)) {
				$Questions = $model->find_many();
				$result = new stdClass();
				foreach ($Questions as $question) {
					$res = array();
					foreach ($getArray as $id) {
						$res->{$id} = $question->$id;
					}
					$result[] = $res;
				}
				return $result;
			} elseif(is_string($getArray)) {
				$Questions = $model->find_many();
				$result = array();
				foreach ($Questions as $question) {
					$result[] = $question->$getArray;
				}
				return $result;
			} else {
				return $model->find_array();
			}
		}
	}

	public static function associate_by($data,$column,$array=false) {
		$output = array();
		foreach ($data as $value) {
			if($array) {
				$output[$value[$column]] = $value;
			} else {
				$output[$value->$column] = $value;
			}
		}
		return $output;
	}

	public static function humanTiming($time) {
		if(gettype($time)=='string') { $time = strtotime($time); }
		$time = time() - $time; // to get the time since that moment
		$time = ($time<1)? 1 : $time;
		$tokens = array (
			31536000 => 'año',
			2592000 => 'mes',
			604800 => 'semana',
			86400 => 'día',
			3600 => 'hora',
			60 => 'minuto',
			1 => 'segundo'
		);

		foreach ($tokens as $unit => $text) {
			if ($time < $unit) continue;
			$numberOfUnits = floor($time / $unit);
			$plural = ($text == 'mes') ? 'es' : 's';
			return 'Hace '.$numberOfUnits.' '.$text.(($numberOfUnits>1)?$plural:'');
		}
	}

	public static function humanTimingEnh($time)
	{
		$curr_date=date("Y-m-d");
		$yester_date = date("Y-m-d", time() - 60 * 60 * 24);
		$the_date=date("Y-m-d",strtotime($time));
		if($curr_date==$the_date) {
			return "Hoy";
		}
		if($yester_date==$the_date) {
			return "Ayer";
		}
		return Helpers::humanTiming($time);
	}

	public static function humanFilesize($bytes, $decimals = 1) {
		$sz = 'BKMGTP';
		$factor = floor((strlen($bytes) - 1) / 3);
		return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor] . 'B';
	}

	public static function getExtImg($filename) {
		$extension = end((explode('.',$filename)));
		return URL."assets/img/file/".$extension.".png";
	}

	public static function stripAccents($str){
		return strtr(utf8_decode($str), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
	}

	public static function onlyChars($str){
		return preg_replace("/[^A-Za-z0-9 ]/", '', self::stripAccents($str));
	}

	public static function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
		$sort_col = array();
		foreach ($arr as $key=> $row) {
			$sort_col[$key] = $row[$col];
		}
		array_multisort($sort_col, $dir, $arr);
	}
}
