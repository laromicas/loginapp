<?php

/**
 * class Login
 * handles the user's login, logout, password changing...
 * 
 */

class Login
{
	private $loginColumn = 'email';

	private $passwordHashColumn = 'password';

	private $activeUserColumn = 'status';
	private $hashConfirmacion = 'hash';

	private $socialIdentifier = 'identifier';

	public $errors = array();

	public function __construct() {
		$this->model = Model::factory('Users');
		$this->socialModel = Model::factory('UsersSocials');
	}

	/**
	 * Makes Login Process and set session variables
	 *
	 * @param string $username Username to check
	 * @param string $pass Password to check
	 */
	public function login($username,$password) {
		if (!empty($username) && !empty($password)) {
			if ($user = $this->password_verify($username,$password)) {
				Session::init();
				Auth::set_current_user($user);

				return $user;
			} else {
				return false;
			}
		} elseif (empty($username)) {
			Session::set('user_logged_in', false);
			$this->errors[] = "Empty username";
			return false;
		} elseif (empty($password)) {
			Session::set('user_logged_in', false);
			$this->errors[] = "Empty password";
			return false;
		}
	}

	/**
	 * Deletes user name from session
	 *
	 */
	public function logout() {
		Session::set('user_data',false);
		Session::set('user_logged_in',false);
		Session::destroy();
	}

	/**
	 * Manages Social Login
	 *
	 */
	public function social_login($username, $socialId) {
		$user = $this->load_model_from_social($username, $socialId);
		if(!$user) {
			$this->errors[] = "Error getting user from social, contact administrator";
			return false;
		}
		if ($user->{$this->activeUserColumn} == 'D') {
			$this->errors[] = "User doesn't exists";
			return false;
		}

		Session::init();
		Auth::set_current_user($user);
		return true;
	}

	/**
	 * Brings user from social
	 *
	 */
	private function load_model_from_social($email,$id) {
		$social = $this->socialModel
			->where_equal($this->socialIdentifier, $id)
			->where_equal($this->loginColumn, $email)
			->find_one();
		if(!$social) {
			return false;
		}
		return $social->User();
	}

		
	/**
	 * Verifies username and password:
	 *
	 * @param string $username Username to check
	 * @param string $pass Password to check
	 */
	public function password_verify($username,$password) {
		$user = $this->model
			->select('users.*')
			->where($this->loginColumn, $username)
			->find_one();
		if ($user) {
				if (!$this->password_verify_bcrypt($password, $user->{$this->passwordHashColumn})) {
					$this->errors[] = "Wrong Password";
				}
				if ($user->{$this->activeUserColumn} == 'D') {
					$this->errors[] = "User doesn't exists";
				}
				if ($user->{$this->activeUserColumn} == 'P') {
					$this->errors[] = "User is pending activation";
				}
				if(count($this->errors)) {
					return false;
				}
				return $user;
		} else {
			$this->errors[] = "User doesn't exists";
			return false;
		}
	}

	/**
	 * Uses php password_verify function, version > 5.5 uses bcrypt:
	 *
	 * @param string $pass Password to check
	 * @param string $hash Hash to check password against
	 */
	private function password_verify_bcrypt($pass, $hash)
	{
		return (password_verify($pass, $hash));
	}

	/**
	 * Change current user password
	 *
	 * @param string $username User ID
	 * @param string $pass New Password to save
	 */
	public function change_password($username,$password)
	{
		$user = $this->model
			->select('users.*')
			->where($this->loginColumn, $username)
			->find_one();
		if(!$user) {
			$this->errors[] = "User doesn't exists";
			return false;
		}
		$user->{$this->passwordHashColumn} = password_hash($password,PASSWORD_BCRYPT, ["cost" => HASH_COST_FACTOR]);
		$user->save();
		return true;
	}
}