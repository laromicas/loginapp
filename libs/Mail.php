<?php
/**
 * class Mail
 * PHPMailer wrapper
 * 
 */
class Mail
{
	
	public $Mailer = '';
	protected $template = 'registro';
	public $error;
	protected $title;
	protected $subject;
	protected $advertising;
	protected $_user;
	protected $copyright;

	protected $from;
	protected $to = array();
	protected $cc = array();
	protected $bcc = array();
	protected $replyTo = '';
	protected $message = '';
	protected $body = '';
	protected $attachments = array();
	protected $url = '';
	protected $login_url = '';
	protected $images = array();
	

	function __construct($params = array('mailer' => '')) {
		if (@$params['mailer']) {
			$this->Mailer = $params['mailer'];
		} else {
			$this->Mailer = new PHPMailer;
		}
		if (@$params['user']) {
			$this->_user = $params['user'];
		}
		$this->Mailer->isHTML(true);


		$this->Mailer->AddBCC("lacides@gmail.com",'Lak');
		$this->url = 'http://'.$_SERVER['SERVER_NAME'].(($_SERVER['SERVER_PORT'] != '80') ? ':'.$_SERVER['SERVER_PORT'] : '' ).URL;
		$this->copyright = '©Lacides Miranda laromicas.com';
	}

	/**
	 * Makes Login Process and set session variables
	 *
	 * @param array $vars Variables to put inside the template to build the mail
	 */
	public function putVars($vars = array()) {
		foreach ($vars as $key => $value) {
			$this->vars[$key] = $value;
		}
	}

	/**
	 * Sends the message
	 *
	 */
	public function send() {
		if(!$this->message) {
			$this->prepareMessage();
		}
		$this->prepare();
		if(!$this->Mailer->Send()) {
			$this->error = 'Error sending email: ' . $this->Mailer->ErrorInfo;
			return false;
		} else {
			$this->error = 'E-Mail sent successfully.';
			return true;
		}
	}

	/**
	 * Prepares the message, sender, embedimages, make sure of ISO-8859-1 use
	 *
	 */
	private function prepare() {
		$this->addLogo();
		$this->addTo($this->_user->email, $this->_user->getFullName());
		$this->from = 'from@example.org';
		$this->replyTo = 'no-reply.from@example.org';


		if(!is_array($this->to)) {
			if($this->to)
				$this->to = array($this->to);
			else
				$this->to = array();
		}
		if(!is_array($this->cc)) {
			if($this->cc)
				$this->cc = array($this->cc);
			else
				$this->cc = array();
		}
		if(!is_array($this->bcc)) {
			if($this->bcc)
				$this->bcc = array($this->bcc);
			else
				$this->bcc = array();
		}


		if(is_array($this->from)) {
			$this->Mailer->From = $this->from[0];
			if((@$this->from[1])) {
				$this->Mailer->FromName = $this->from[1];
			}
		} elseif(is_string($this->replyTo)) {
			$this->Mailer->From = $this->from;
		} else {
			$this->Mailer->From = Helpers::getConf('NO_REPLY','EMAIL',0);
			$this->Mailer->FromName = Helpers::getConf('NO_REPLY_NAME','EMAIL');
		}

		$replyAddress = 'no-reply.from@example.org';
		$replyName = 'Clickdelivery Test App';

		$this->Mailer->AddReplyTo($replyAddress, $replyName);
		foreach ($this->to as $mail) {
			if(is_array($mail)) {
				$this->Mailer->AddAddress($mail[0],$mail[1]);
				$this->Mailer->AddCC($mail[0],$mail[1]);
			} else {
				$this->Mailer->AddAddress($mail);
				$this->Mailer->AddCC($mail);
			}
		}
		foreach ($this->cc as $mail) {
			if(is_array($mail)) {
				$this->Mailer->AddCC($mail[0],$mail[1]);
			} else {
				$this->Mailer->AddCC($mail);
			}
		}
		foreach ($this->bcc as $mail) {
			$this->Mailer->AddBCC($mail);
		}

		$this->emmbedImages();
		foreach ($this->attachments as $file) {
			$this->Mailer->AddAttachment($file['path'], $file['name'], $file['encoding'], $file['type']);
		}

		// var_dump($this->Mailer);die();

		$this->Mailer->Subject = $this->subject;
		$this->Mailer->Body = iconv('UTF-8','ISO-8859-1//TRANSLIT//IGNORE', $this->message);
		$this->Mailer->AltBody = iconv('UTF-8','ISO-8859-1//TRANSLIT//IGNORE', 'Cosmopolitan Staffing');
	}

	/**
	 * Add images to stack before adding, this way you can preview mail before sending
	 *
	 * @param string $image name of the image file
	 * @param string $name name of the image inside the mail
	 * @param string $path path to the image file
	 * @param string $url url to the image file for preview
	 */
	protected function addImage($image,$name,$path,$url) {
		$this->images[] = array(
			"image" => $image,
			"name" => $name,
			"path" => $path,
			"url" => $url
			);
	}

	/**
	 * emmbed images to send
	 *
	 */
	private function emmbedImages() {
		 // TODO Pruebas para enviarlos
		foreach ($this->images as $image) {
			if(file_exists($image["path"].$image["image"])) {
				$this->Mailer->AddEmbeddedImage($image["path"].$image["image"],$image["name"]);
			}
		}
		
	}

	/**
	 * Add attachment to stack before sending
	 *
	 */
	protected function addAttachment($path, $name = '', $encoding = 'base64', $type = 'application/octet-stream') {
		// $this->Mailer->AddAttachment($file, $name, $encoding, $type);
		$this->attachments[] = array('path' => $path, 'name' => $name, 'encoding' => $encoding, 'type' => $type);
	}

	/**
	 * Get HTML to preview mail, make an echo
	 *
	 */
	public function get_html() {
		$this->addLogo();
		if(!$this->message) {
			$this->prepareMessage();
		}
		$html = $this->message;
		// $html = iconv('UTF-8','ISO-8859-1//TRANSLIT//IGNORE', $this->message);
		foreach ($this->images as $image) {
			$html = str_replace('"cid:'.$image["name"].'"', '"'.$image["url"].$image["image"].'"', $html);
		}
		return $html;
	}

	/**
	 * Add default logo to stack
	 *
	 */
	public function addLogo() {
		$logo = 'favicon.png';
		$this->addImage($logo, "logo", ASSETS_PATH.'img/', ASSETS_URL.'img/');
		// var_dump($this->images);
	}

	/**
	 * Add several Recipients to sending
	 *
	 */
	public function addManyTo($emails) {
		foreach ($emails as $email) {
			$this->addTo($email['email'],$email['name']);
		}
	}

	/**
	 * Add Recipient to stack
	 *
	 */
	public function addTo($to, $to_name = "") {
		if(self::isEmail($to)) {
			if($to_name) {
				$this->to[] = array($to, $to_name);
			} else {
				$this->to[] = array($to);
			}
		}
	}

	/**
	 * Add Carbon Copy Recipient to stack
	 *
	 */
	public function addCc($cc, $cc_name = "") {
		if(self::isEmail($cc)) {
			if($cc_name) {
				$this->cc[] = array($cc, $cc_name);
			} else {
				$this->cc[] = array($cc);
			}
		}
	}

	/**
	 * Add Blind Carbon Copy Recipient to stack
	 *
	 */
	public function addBcc($bcc, $bcc_name = "") {
		if(self::isEmail($bcc)) {
			if($bcc_name) {
				$this->bcc[] = array($bcc, $bcc_name);
			} else {
				$this->bcc[] = array($bcc);
			}
		}
	}

	/**
	 * Prepares the body of the message putting all vars
	 *
	 */
	public function prepareMessage() {
		ob_start();
		require 'libs/Mail/'.$this->template;
		$this->body = ob_get_clean();
		if (ob_get_length()) ob_end_clean();
		// die();
		ob_start();
		require 'libs/Mail/template.php';
		$this->message = ob_get_clean();
		if (ob_get_length()) ob_end_clean();
	}

	/**
	 * Checks if a email is valid before putting in stacks
	 *
	 */
	public static function isEmail($mail) {
		return filter_var($mail, FILTER_VALIDATE_EMAIL);
		// return (preg_match("/(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/", $email) || !preg_match("/^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/", $email)) ? false : true;
	}
}