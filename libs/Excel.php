<?php

/**
 * class Excel
 * handles the import and export from/to files...
 * 
 */

class Excel
{

	private $type;
	private $objPHPExcel;

	public function __construct($type = 'xls') {
		$this->type = $type;
	}

	/*
	*	Recolects all info of users and update PHPExcel Object
	*/
	public function getUsers() {
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 3600);

		$users = Model::factory('Users')->where_gt('user_id',1)->find_many();
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getProperties()->setCreator("Lak Mir");
		$objPHPExcel->getProperties()->setLastModifiedBy("Clickdelivery");
		$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Products Export");
		$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Products Export");
		$objPHPExcel->getProperties()->setDescription("Export for Clickdelivery Login App");

		$objPHPExcel->setActiveSheetIndex(0);
		$sheet = $objPHPExcel->getActiveSheet();
		$sheet->getSheetView()->setZoomScale(80);

		$sheet->setTitle('Users');

		$columns = array('user_id','email','first_name','last_name','phone','mobile','address','city','region','country','zip','type','status','date_created','date_updated','last_connect');

		$col = 0;
		$row = 1;
		foreach ($columns as $value) {
			$sheet->setCellValueByColumnAndRow($col, $row, $value); $col++;
		}

		
		foreach ($users as $user) {
			$row++;
			$col = 0;
			foreach ($columns as $value) {
				$sheet->setCellValueByColumnAndRow($col, $row, $user->{$value});  $col++;
			}
		}
		$this->objPHPExcel = $objPHPExcel;
		return $objPHPExcel;
	}

	/*
	*	Outputs the PHPExcel Object to browser or file
	*/
	public function output($file = 'php://output') {
		switch ($this->type) {
			case 'xlsx':
					header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					$objWriter = new PHPExcel_Writer_Excel2007($this->objPHPExcel);
					header("Content-Disposition: attachment; filename=\"users.xlsx\";" );
				break;
			case 'xls':
					header("Content-Type: application/vnd.ms-excel");
					$objWriter = new PHPExcel_Writer_Excel5($this->objPHPExcel);
					header("Content-Disposition: attachment; filename=\"users.xls\";" );
				break;
			case 'csv':
					header("Content-Type: text/csv");
					$objWriter = new PHPExcel_Writer_CSV($this->objPHPExcel);
					$objWriter->setDelimiter(',');
					header("Content-Disposition: attachment; filename=\"users.csv\";" );
				break;
			case 'tsv':
					header("Content-Type: text/csv");
					$objWriter = new PHPExcel_Writer_CSV($this->objPHPExcel);
					$objWriter->setDelimiter("\t");
					header("Content-Disposition: attachment; filename=\"users.tsv\";" );
				break;
			default:
				# code...
				break;
		}
		$objWriter->save($file);
	}

	/*
	*	Import users from XLS and XLSX
	*/
	public function importUsersExcel($file, $extension) {
		if(!in_array($extension,array('xlsx','xls'))) {
			return $this->importUsersCsv($file, $extension);
		} else {
			$objPHPExcel = PHPExcel_IOFactory::load($file);
			$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,false,false,true);
		}
		$cols = array_flip($sheetData[1]);
		$columns = array('email','first_name','last_name','phone','mobile','address','city','region','country','zip','type','status','date_created','date_updated','last_connect');
		$required = array('email','first_name','last_name');
		$titulos = $sheetData[1];

		$keys = array_keys($titulos);	//Get last column to put errors
		$last_key = array_pop($keys);
		$key_errors = ++$last_key;
		$errors = array();
		$sheetData[1][$key_errors] = 'Errors';
		$errors[] = $sheetData[1];

		foreach ($required as $key => $value) {
			if(!in_array($value, $titulos)) {
				$errors[] = array('A' => "Column ".$value." missing.");
			}
		}

		$isFirst = true;
		foreach ($sheetData as $row) {
			if ($isFirst) {	//Doesn't touch titles
				$isFirst = false;
				continue;
			}

			foreach ($required as $key => $value) {
				if(!@$row[@$cols[$value]]) {
					$row[$key_errors] = $value." ".$row[$cols[$value]]." missing"; $errors[] = $row;
					continue;
				}
			}
			
			$user = Model::factory('Users')->where('email',$row[$cols['email']])->find_one();
			if(!$user) {
				$user = Model::factory('Users')->create();
				$user->status = 'P';	//Default values for status and type (if set in excel they are overwritten)
				$user->type = 'C';
			}
			foreach ($titulos as $titulo) {
				if(in_array($titulo, $columns) && $row[$cols[$titulo]]) {
					$user->{$titulo} = $row[$cols[$titulo]];
				}
			}
			if(!$user->save()) {
				$row[$key_errors] = "Error al salvar"; $errors[] = $row; continue;
			}
		}
		return $errors;
	}

	/*
	*	Import users from CSV and TSV, faster than using PHPExcel
	*/
	public function importUsersCsv($file, $extension) {
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 600);
		ini_set("auto_detect_line_endings", true);


		if (($handle = fopen($inputFileName, "r")) !== FALSE) {	//Autodetect separator
			$data = fgets($handle, 4096);
			$csv = explode(';', $data);
			if(count($csv) > 1) {
				$delimiter = ';';
			} else {
				$csv = explode(',', $data);
				if(count($csv) > 1) {
					$delimiter = ',';
				} else {
					$csv = explode("\t", $data);
					if(count($csv) > 1) {
						$delimiter = "\t";
					} else {
						return array(array('Error en archivo'));
					}
				}
			}
		} else {
			return array(array('Error en archivo'));
		}


		$cols = array_flip($csv);
		$columns = array('email','first_name','last_name','phone','mobile','address','city','region','country','zip','type','status','date_created','date_updated','last_connect');
		$required = array('email','first_name','last_name');
		$titulos = $sheetData[1];

		$keys = array_keys($titulos);	//Get last column to put errors
		$last_key = array_pop($keys);
		$key_errors = ++$last_key;
		$errors = array();
		$csv[$key_errors] = 'Errors';
		$errors[] = $csv;

		foreach ($required as $key => $value) {
			if(!in_array($value, $titulos)) {
				$errors[] = array('A' => "Column ".$value." missing.");
			}
		}
	
		$isFirst = true;

		while (($row = fgetcsv($handle, 4096, $delimiter)) !== FALSE) { //Takes row by row and turn them into array
			if ($isFirst) {	//Doesn't touch titles
				$isFirst = false;
				continue;
			}

			foreach ($required as $key => $value) {
				if(!@$row[@$cols[$value]]) {
					$row[$key_errors] = $value." ".$row[$cols[$value]]." missing"; $errors[] = $row;
					continue;
				}
			}
			
			$user = Model::factory('Users')->where('email',$row[$cols['email']])->find_one();
			if(!$user) {
				$user = Model::factory('Users')->create();
				$user->status = 'P';	//Default values for status and type (if set in excel they are overwritten)
				$user->type = 'C';
			}
			foreach ($titulos as $titulo) {
				if(in_array($titulo, $columns) && $row[$cols[$titulo]]) {
					$user->{$titulo} = $row[$cols[$titulo]];
				}
			}
			if(!$user->save()) {
				$row[$key_errors] = "Error al salvar"; $errors[] = $row; continue;
			}
		}
		return $errors;
	}

	/*
	*	Save file with errors resulting from importing
	*/
	public function saveErrors($errors, $file = 'php://output', $type = 'csv') {
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 600);
		if($type == 'json') {
			file_put_contents($file.'.'.$type, json_encode($errors));
			return end(explode('/', $file)).'.'.$type;
		}
		if($type == 'csv' || $type == 'tsv') {
			switch ($type) {
				case 'csv':
					$delimiter = ',';
					break;
				case 'tsv':
					$delimiter = "\t";
					break;
			}
			$fp = fopen($file.'.'.$type, "wb");
			foreach ($errors as $line) {
				if(!is_array($line)) { $line = array($line); }
				fputcsv($fp, $line, $delimiter);
			}
			fclose($fp);
			$f = explode('/', $file);
			$f = end($f);
			return $f.'.'.$type;
		}

		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getProperties()->setCreator("Lak Mir");
		$objPHPExcel->getProperties()->setLastModifiedBy("Clickdelivery");
		$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Products Export");
		$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Products Export");
		$objPHPExcel->getProperties()->setDescription("Errors for Clickdelivery Login App");

		$objPHPExcel->setActiveSheetIndex(0);
		$sheet = $objPHPExcel->getActiveSheet();
		$sheet->getSheetView()->setZoomScale(80);

		$sheet->setTitle('Errors');

		$sheet->fromArray($errors, NULL, 'A1');

		$sheet->getColumnDimension('C')->setWidth(50);
		$sheet->getColumnDimension('Y')->setWidth(50);

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($file.'.'.$type);
		$file = explode('/', $file);
		return end($file).'.'.$type;
	}

}
