<?php

class Auth {
	public static function handleLogin() {
		Session::init();
		if (Session::get('user_logged_in') == false) {
			Session::set('URI',$_SERVER['REQUEST_URI']);
			Html::redirect_to(URL.'login/index');
			exit();
		}
		return Session::get("user_data");
	}

	/**
	 * Sets session with user data
	 *
	 * @param string $user_data
	 */

	public static function set_current_user($user_data = '') {
		if($user_data) {
			Session::set('user_logged_in', true);
			Session::set('user_data',$user_data);
			Session::set('user_login', $user_data->email);
			Session::set('user_name', $user_data->first_name.' '.$user_data->last_name);

		} else {
			return false;
		}
	}


	/**
	 * Gets user data from session
	 *
	 * @param string $user_data
	 */
	public static function current_user($user_data) {
		return Session::get('user_data');
	}


	/**
	 * Detects if user has access
	 *
	 * @param string $al Access List ex: AGC
	 */
	public static function checkAccess($al) {
		$user = Session::get("user_data");
		if (Session::get('user_logged_in')) {
			if ($user->type == 'A') {
				return true;
			}
			if(strpos(@$al, $user->type) !== false) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Detects if user has access and redirects to error if not
	 *
	 * @param string $al Access List ex: AGC
	 */
	public static function handleAccess($al) {
		$user = Session::get("user_data");
		if (Session::get('user_logged_in')) {
			if ($user->type == 'A') {
				return true;
			}
			if(strpos(@$al, $user->type) !== false) {
				return true;
			}
		}
		Html::redirect_to(URL);
		die();
	}
}