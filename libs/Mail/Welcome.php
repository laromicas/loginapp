<?php
/**
* 
Obligatorios (para mensaje (antes de send)):
	$this->subject
	$vars['short_message']
	$vars['name']
	$vars['url']
	$vars['message']

Obligatorios (Para configuracion):
	$this->template
	$this->putVars($vars);



*/
class Mail_Welcome extends Mail
{
	function __construct($user, $send_reset_link = false)
	{
		parent::__construct(array('user'=>$user));
		$this->subject = "Register Confirmation";
		$this->template = 'template_register.php';

		$activation_link = 'users/activation_link/'.$this->_user->email.'/'.$this->_user->hash;
		$vars = array(
			//Todos los mensajes
			'short_message' => 'Welcome to Click App Account Registration.',
			'url' => 'mail/welcome/'.$this->_user->email.'/'.$this->_user->hash,
			'activation_link' => $activation_link,
			'message' => 'Welcome to Click App Account Registration. '
			);
		$this->putVars($vars);
	}
}