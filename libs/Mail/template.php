<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title><?= $this->subject ?></title>
<style type="text/css">
#outlook a{padding:0;}body{width:100%!important;}.ReadMsgBody{width:100%;}.ExternalClass{width:100%;}body{-webkit-text-size-adjust:none;}body{margin:0;padding:0;}img{border:0;height:auto;line-height:100%;outline:none;text-decoration:none;}table td{border-collapse:collapse;}#backgroundTable{height:100%!important;margin:0;padding:0;width:100%!important;}body,#backgroundTable{background-color:#EEEEEE;}#templateContainer{border:1px solid #BBBBBB;}h1,.h1{
	color: #FF9900;
	display: block;
	font-family: Arial;
	font-size: 40px;
	font-weight: bold;
	line-height: 100%;
	margin-top: 2%;
	margin-right: 0;
	margin-bottom: 1%;
	margin-left: 0;
	text-align: left;
}h2,.h2{color:#404040;display:block;font-family:Arial;font-size:18px;font-weight:bold;line-height:100%;margin-top:2%;margin-right:0;margin-bottom:1%;margin-left:0;text-align:left;}h3,.h3{color:#606060;display:block;font-family:Arial;font-size:16px;font-weight:bold;line-height:100%;margin-top:2%;margin-right:0;margin-bottom:1%;margin-left:0;text-align:left;}h4,.h4{color:#808080;display:block;font-family:Arial;font-size:14px;font-weight:bold;line-height:100%;margin-top:2%;margin-right:0;margin-bottom:1%;margin-left:0;text-align:left;}#templatePreheader{background-color:#eeeeee;}.preheaderContent div{color:#707070;font-family:Arial;font-size:10px;line-height:100%;text-align:left;}.preheaderContent div a:link,.preheaderContent div a:visited,.preheaderContent div a .yshortcuts{color:#3498db;font-weight:normal;text-decoration:underline;}#social div{text-align:right;}#templateHeader{background-color:#FFFFFF;border-bottom:5px solid #505050;}.headerContent{color:#202020;font-family:Arial;font-size:34px;font-weight:bold;line-height:100%;padding:10px;text-align:right;vertical-align:middle;}.headerContent a:link,.headerContent a:visited,.headerContent a .yshortcuts{color:#3498db;font-weight:normal;text-decoration:underline;}#headerImage{height:auto;max-width:600px!important;}#templateContainer,.bodyContent{background-color:#FDFDFD;}.bodyContent div{color:#505050;font-family:Arial;font-size:14px;line-height:150%;text-align:justify;}.bodyContent div a:link,.bodyContent div a:visited,.bodyContent div a .yshortcuts{color:#3498db;font-weight:normal;text-decoration:underline;}.bodyContent img{display:inline;height:auto;}#templateSidebar{background-color:#FDFDFD;}.sidebarContent{border-left:1px solid #DDDDDD;}.sidebarContent div{color:#505050;font-family:Arial;font-size:10px;line-height:150%;text-align:left;}.sidebarContent div a:link,.sidebarContent div a:visited,.sidebarContent div a .yshortcuts{color:#3498db;font-weight:normal;text-decoration:underline;}.sidebarContent img{display:inline;height:auto;}#templateFooter{background-color:#FAFAFA;border-top:3px solid #909090;}.footerContent div{color:#707070;font-family:Arial;font-size:11px;line-height:125%;text-align:left;}.footerContent div a:link,.footerContent div a:visited,.footerContent div a .yshortcuts{color:#3498db;font-weight:normal;text-decoration:underline;}.footerContent img{display:inline;}#social{background-color:#FFFFFF;border:0;}#social div{text-align:left;}#utility{background-color:#FAFAFA;border-top:0;}#utility div{text-align:left;}#monkeyRewards img{max-width:170px!important;}
</style>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<center>
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable">
<tr>
<td align="center" valign="top">
 
<table border="0" cellpadding="10" cellspacing="0" width="600" id="templatePreheader">
<tr>
<td valign="top" class="preheaderContent">
 
<table border="0" cellpadding="10" cellspacing="0" width="100%">
<tr>
<td valign="top">
<div mc:edit="std_preheader_content">
<?= $this->vars['short_message'] ?>
</div>
</td>
 
<td valign="top" width="170">
<div mc:edit="std_preheader_links">
<?php if (@$this->vars['url']): ?>
Can't see this Email?<br/><a href="<?= $this->url.$this->vars['url'] ?>" target="_blank">Click here to see in browser</a>.
<?php endif ?>
</div>
</td>
 
</tr>
</table>
 
</td>
</tr>
</table>
 
<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
<tr>
<td align="center" valign="top">
 
<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
<tr>
<td class="headerContent" width="100%" style="padding-left:20px; padding-right:10px;">
<div mc:edit="Header_content">
<h1>Welcome, <?= $this->_user->first_name ?></h1>
</div>
</td>
<td class="headerContent">
<img src="cid:logo" style="max-height:80px;" id="headerImage campaign-icon" mc:label="header_image" mc:edit="header_image" mc:allowtext />
</td>
</tr>
</table>
 
</td>
</tr>
<tr>
<td align="center" valign="top">
 
<table border="0" cellpadding="10" cellspacing="0" width="600" id="templateBody">
<tr>
<td valign="top" class="bodyContent">
 
<table border="0" cellpadding="10" cellspacing="0" width="100%">
<tr>
<td valign="top" style="padding-right:0;">

<?= $this->body ?>

</td>
</tr>
</table>
 
</td>
</tr>
</table>
 
</td>
</tr>
<tr>
<td align="center" valign="top">
 
<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateFooter">
<tr>
<td valign="top" class="footerContent">
 
<table border="0" cellpadding="10" cellspacing="0" width="100%">
<!-- <tr>
<td colspan="2" valign="middle" id="social">
<div mc:edit="std_social">
&nbsp;<a href="*|TWITTER:PROFILEURL|*">follow on Twitter</a> | <a href="*|FACEBOOK:PROFILEURL|*">friend on Facebook</a> | <a href="*|FORWARD|*">forward to a friend</a>&nbsp;
</div>
</td>
</tr> -->
<tr>
<td valign="top" width="350">
<div mc:edit="std_footer">
<em>Copyright &copy; <?= $this->copyright ?></em>
<!-- <br/>
*|IFNOT:ARCHIVE_PAGE|* *|LIST:DESCRIPTION|*
<br/>
<strong>Our mailing address is:</strong>
<br/>
*|HTML:LIST_ADDRESS_HTML|**|END:IF|* -->
</div>
</td>
<td valign="top" width="190" id="monkeyRewards">
<!-- <div mc:edit="monkeyrewards">
*|IF:REWARDS|* *|HTML:REWARDS|* *|END:IF|*
</div> -->
</td>
</tr>
<!-- <tr>
<td colspan="2" valign="middle" id="utility">
<div mc:edit="std_utility">
&nbsp;<a href="*|UNSUB|*">unsubscribe from this list</a> | <a href="*|UPDATE_PROFILE|*">update subscription preferences</a>&nbsp;
</div>
</td>
</tr> -->
</table>
 
</td>
</tr>
</table>
 
</td>
</tr>
</table>
<br/>
</td>
</tr>
</table>
</center>
</body>
</html>