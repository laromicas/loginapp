<!DOCTYPE html>
<!-- 
Template Name: <?= APP_TITLE ?> - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/<?= APP_TITLE ?>-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/<?= APP_TITLE ?>-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title><?= APP_TITLE ?> App</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="" name="author" />
		<!-- BEGIN GLOBAL MANDATORY STYLES -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
		<link href="<?= URL ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= URL ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= URL ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= URL ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<!-- END GLOBAL MANDATORY STYLES -->
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="<?= URL ?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= URL ?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		<!-- END PAGE LEVEL PLUGINS -->
		<!-- BEGIN THEME GLOBAL STYLES -->
		<link href="<?= URL ?>assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
		<link href="<?= URL ?>assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
		<!-- END THEME GLOBAL STYLES -->
		<!-- BEGIN PAGE LEVEL STYLES -->
		<link href="<?= URL ?>assets/pages/css/login.min.css" rel="stylesheet" type="text/css" />
		<!-- END PAGE LEVEL STYLES -->
		<!-- BEGIN THEME LAYOUT STYLES -->
		<!-- END THEME LAYOUT STYLES -->
		<!-- <link rel="shortcut icon" href="favicon.ico" /> -->
		<link rel="shortcut icon" href="<?= URL ?>assets/img/favicon.png" />
	</head>

	<!-- END HEAD -->

	<body class=" login">
		<!-- BEGIN LOGO -->
		<div class="logo">
			<a href="index.html">
				<img src="<?= URL ?>assets/img/logo.png" alt="" style="height: 50px"> </a>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN LOGIN -->
		<div class="content">
						<!-- BEGIN LOGIN FORM -->
			<form class="login-form" action="<?= URL ?>users/register" method="post">
				<h3 class="form-title font-green">Your registration has been successful, you will receive a mail promptly to activate your account.</h3>
			</form>
			<form class="login-form" action="<?= htmlspecialchars($this->loginUrl) ?>" method="get">
				<h4 style="text-align:center">Or Activate with facebook.</h4><small>You must have the same mail you used to register your account.</small>
				<div class="form-actions" style="text-align:center">
					<a href="<?= htmlspecialchars($this->loginUrl) ?>" class="btn green uppercase facebook_button">
						<i class="fa fa-facebook"></i>
						Activate with Facebook
					</a>
				</div>
				<input type="hidden" name="token" id="token">
			</form>
			<!-- END LOGIN FORM -->
		</div>
		<div class="copyright"> 2017 © <a target="_blank" href="http://laromicas.com">Lacides Miranda</a> Not affiliated with clickdelivery. only for testing.</div>
		<!--[if lt IE 9]>
<script src="<?= URL ?>assets/global/plugins/respond.min.js"></script>
<script src="<?= URL ?>assets/global/plugins/excanvas.min.js"></script> 
<script src="<?= URL ?>assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
		<!-- BEGIN CORE PLUGINS -->
		<script src="<?= URL ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

		<script src="<?= URL ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?= URL ?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
		<script src="<?= URL ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
		<script src="<?= URL ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
		<script src="<?= URL ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
		<!-- END CORE PLUGINS -->
		<!-- BEGIN THEME GLOBAL SCRIPTS -->
		<script src="<?= URL ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
		<!-- END THEME GLOBAL SCRIPTS -->
		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?= URL ?>assets/js/konami.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->
	</body>
</html>