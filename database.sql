-- Volcando estructura de base de datos para loginapp
CREATE DATABASE IF NOT EXISTS `loginapp` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `loginapp`;

-- Volcando estructura para tabla loginapp.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(70) DEFAULT NULL,
  `hash` varchar(70) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,   -- for next versions
  `city` varchar(20) DEFAULT NULL,      -- for next versions
  `region` varchar(20) DEFAULT NULL,    -- for next versions
  `country` varchar(20) DEFAULT NULL,   -- for next versions
  `zip` varchar(10) DEFAULT NULL,       -- for next versions
  `type` CHAR(1) NOT NULL DEFAULT 'C' COMMENT 'Admin, aGent, Customer',
  `status` CHAR(1) NOT NULL DEFAULT 'P' COMMENT 'Active, Pending Activation, Disabled',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_connect` timestamp NULL DEFAULT NULL,
  `hash_date` datetime DEFAULT NULL,    -- for next versions
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`),
  KEY `type` (`type`),
  KEY `first_name` (`first_name`),
  KEY `last_name` (`last_name`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Volcando estructura para tabla loginapp.users_socials
CREATE TABLE IF NOT EXISTS `users_socials` (
  `users_socials_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `provider` varchar(10) DEFAULT NULL,
  `identifier` varchar(50) DEFAULT NULL,
  `profileURL` varchar(200) DEFAULT NULL,
  `photoURL` varchar(200) DEFAULT NULL,
  `displayName` varchar(50) DEFAULT NULL,
  `firstName` varchar(25) DEFAULT NULL,
  `lastName` varchar(25) DEFAULT NULL,
  `gender` varchar(15) DEFAULT NULL,
  `language` varchar(7) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `verified` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`users_socials_id`),
  KEY `user_provider` (`user_id`,`provider`),
  KEY `identifier` (`provider`,`identifier`),
  KEY `email` (`email`),
  KEY `user` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
