<?php

/**
 * Controller Users
 * handles the users CRUD operations
 * 
 */

class Users_Controller extends Controller {

	public $specialLoad = "true";

	function __construct() {
		parent::__construct();
	}

	/**
	 * Default method allways being called when calling users controller
	 *
	 * @param string $name User Id when needed
	 * @param string $arguments Not Used
	 */
	public function __call($name, $arguments) {
		if($_SERVER['REQUEST_METHOD'] == 'GET') {
			if($name && $name != 'index') {
				$this->read($name);
				return;
			} else {
				$this->ulist();
				return;
			}
		} elseif($_SERVER['REQUEST_METHOD'] == 'POST' && @$_POST['method'] != '_put' && @$_POST['method'] != '_delete') {
			$this->create();
		} elseif($_SERVER['REQUEST_METHOD'] == 'PUT' || @$_POST['method'] == '_put') { //Needed to fake PUT methods when sending files
			$this->update($name);
		} elseif($_SERVER['REQUEST_METHOD'] == 'DELETE' || @$_POST['method'] == '_delete') {
			$this->delete($name);
		}
	}

	/**
	 * Method to list all users
	 *
	 */
	function ulist() {
		Auth::handleAccess('AG'); // Customers cannot list users
		$users = Model::factory('Users')
		->where_gt('user_id',1)
		->find_many();
		// var_dump($users);
		$usersarray = array();
		foreach ($users as $user) {
			$userarray = array(
				'profileImage' => $user->getProfileImage(),
				'name' => $user->getFullName(),
				'user_id' => $user->user_id,
				'first_name' => $user->first_name,
				'last_name' => $user->last_name,
				'email' => $user->email,
				'birth_day' => $user->birth_day,
				'gender' => $user->gender,
				'phone' => $user->phone,
				'mobile' => $user->mobile,
				'address' => $user->address,
				'city' => $user->city,
				'region' => $user->region,
				'country' => $user->country,
				'zip' => $user->zip,
				'type' => $user->type,
				'status' => $user->status,
				'last_connect' => $user->last_connect,
				'date_updated' => $user->date_updated,
				'date_created' => $user->date_created
			);
			$usersarray[] = $userarray;
		}
		echo json_encode($usersarray);
	}


	/**
	 * Method to create a new user, must be called by POST
	 *
	 */
	function create() {
		// Anyone can create/register a new user so there is no Handle Access
		if($_SERVER['REQUEST_METHOD'] != 'POST') {
			return;
		}
		$result = array('error' => 0);
		$user = Model::factory('Users')->create();
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$email = $_POST['email'];
		$phone = @$_POST['phone'];
		if(Auth::checkAccess('A') && @$_POST['type']) {
			$type = @$_POST['type'];	// Only admin users can put type in new user
		} else {
			$type = 'C';				// Default type = Customer
		}			

		if(!$first_name || !$last_name || !$email) {
			$msg = '';
			$msg .= (!$email) ? 'E-Mail Required. ' : '';
			$msg .= (!$first_name) ? 'First Name Required. ' : '';
			$msg .= (!$last_name) ? 'Last Name Required. ' : '';
			$result = array('error' => 1, 'msg' => 'There are Missing Fields. '.$msg);
			echo json_encode($result); return;
		}
		if(!Html::isEmail($email)) {
			$result = array('error' => 1, 'msg' => 'E-Mail not valid');
			echo json_encode($result); return;
		}
		if($ucheck = Model::factory('Users')->where('email',$email)->find_one()) {
			$result = array('error' => 1, 'msg' => 'E-Mail already registered');
			echo json_encode($result); return;
		}
		$user->first_name = $first_name;
		$user->last_name = $last_name;
		$user->email = $email;
		$user->phone = $phone;
		$user->type = $type;
		$user->hash = hash('sha256',mt_rand());		//Hash to check validity of email.
		$user->status = 'P';		// Default Status -> Pending Mail activation.
		if($user->save()) {
			$result['data'] = $user->as_array();
			$result['data']['user_id'] = $user->user_id;
		} else {
			$result = array('error' => 1, 'msg' => 'Unknown Error');
		}
		echo json_encode($result);
	}

	/**
	 * Method to read user data, must be called by GET
	 *
	 * @param string $user_id User ID to be requested
	 */
	function read($user_id) {
		$user = Auth::handleLogin(); //Anyone must be logged on

		if(!Auth::checkAccess('AG') && $user_id != $user->user_id && $user_id != 'current') { //Customers cannot see another users data
			Html:redirect_to(URL); return;
		}
		if($user_id != 'current') {
			$user = Model::factory('Users')->where_id_is($user_id)->find_one();
		}
		if(!$user) {
			echo json_encode(false);
		}
		$userarray = array(
			'profileImage' => $user->getProfileImage(),
			'name' => $user->getFullName(),
			'user_id' => $user->user_id,
			'first_name' => $user->first_name,
			'last_name' => $user->last_name,
			'email' => $user->email,
			'birth_day' => $user->birth_day,
			'gender' => $user->gender,
			'phone' => $user->phone,
			'mobile' => $user->mobile,
			'address' => $user->address,
			'city' => $user->city,
			'region' => $user->region,
			'country' => $user->country,
			'zip' => $user->zip,
			'type' => $user->type,
			'status' => $user->status,
			'last_connect' => $user->last_connect,
			'date_updated' => $user->date_updated,
			'date_created' => $user->date_created
		);
		echo json_encode($userarray);
	}

	/**
	 * Method for update user, must be used by PUT or faked PUT in POST
	 *
	 * @param string $user_id User to be requested
	 */
	function update($user_id) {
		if($_SERVER['REQUEST_METHOD'] != 'PUT' &&  @$_POST['method'] != '_put') {
			return;
		} elseif(@$_POST['method'] == '_put') {
			$_PUT = $_POST;
		} else {
			parse_str(file_get_contents("php://input"),$_PUT);
		}

		$user = Auth::handleLogin(); //Anyone must be logged on
		if(!Auth::checkAccess('A') && $user_id != $user->user_id) { //Only Admin or self can update user data
			Html:redirect_to(URL); return;
		}

		$result = array('error' => 0);
		$user = Model::factory('Users')
			->select_many('user_id','first_name','last_name','email','phone')
			->where_id_is($user_id)->find_one();
		$first_name = $_PUT['first_name'];
		$last_name = $_PUT['last_name'];
		$phone = @$_PUT['phone'];
		$email = @$_PUT['email'];
		$type = @$_PUT['type'];
		$status = @$_PUT['status'];
		if(!$first_name || !$last_name || !$email) {
			$msg = '';
			$msg .= (!$email) ? 'E-Mail Required. ' : '';
			$msg .= (!$first_name) ? 'First Name Required. ' : '';
			$msg .= (!$last_name) ? 'Last Name Required. ' : '';
			$result = array('error' => 1, 'msg' => 'There are Missing Fields. '.$msg);
			echo json_encode($result); return;
		}
		if(!Html::isEmail($email)) {
			$result = array('error' => 1, 'msg' => 'E-Mail not valid');
			echo json_encode($result); return;
		}
		if($ucheck = Model::factory('Users')->where('email',$email)->find_one()) {
			if($ucheck->user_id != $user_id) {
				$result = array('error' => 1, 'msg' => 'E-Mail already registered');
				echo json_encode($result); return;
			}
		}
		$user->first_name = $first_name;
		$user->last_name = $last_name;
		$user->email = $email;
		$user->phone = $phone;
		if(!Auth::checkAccess('A')) { //Only admin can change type and status
			$user->type = $type;
			$user->status = $status;
		}
		if($user->save()) {
			$result['data'] = $user->as_array();
		} else {
			$result = array('error' => 1, 'msg' => 'Unknown Error');
		}
		echo json_encode($result);
	}

	/**
	 * Method for delete user, must be used by DELETE or faked DELETE in POST
	 *
	 * @param string $user_id User to be deleted
	 */
	function delete($user_id) {
		Auth::handleAccess('A'); //Only admins can delete users
		if($_SERVER['REQUEST_METHOD'] != 'DELETE' && @$_POST['method'] != '_delete') {
			return;
		}
		$result = array('error' => 0);
		$user = Model::factory('Users')->where_id_is($user_id)->find_one();
		if($user) {
			$social = $user->Social('Facebook');
			if($social) {
				$social->delete();
			}
			$user->delete();
			$result = array('error' => 0);
		} else {
			$result = array('error' => 1, 'msg' => 'User not found');
		}
		echo json_encode($result);
	}

	/**
	 * Method for changing password, must be used by PUT or faked PUT in POST
	 *
	 * @param string $user_id User to be deleted, needed if not updating self password (admin)
	 */
	function password($user_id = false) {
		$result = array('error' => 0);
		if($_SERVER['REQUEST_METHOD'] != 'PUT' &&  @$_POST['method'] != '_put') {
			return;
		} elseif(@$_POST['method'] == '_put') {
			$_PUT = $_POST;
		} else {
			parse_str(file_get_contents("php://input"),$_PUT);
		}

		$user = Auth::handleLogin();	//Only logged users can change password
		$user_id = ($user_id) ? $user_id : $user->user_id;
		if(!Auth::checkAccess('A') && $user->user_id != $user_id) {	//If users is not an admin, he can change only personal password
			Html:redirect_to(URL); return;
		}

		$password = $_PUT['password'];

		$login = new Login();
		if(!Auth::checkAccess('A')) { //If user is not an admin system checks old password
			if(!$login->password_verify($user->email, $password['old']) && $user->password != '') {
				$result = array('error' => 1, 'msg' => 'Wrong Current Password');
				echo json_encode($result);
				exit();
			}
		}

		if(!$password['new']) {
			$result = array('error' => 1, 'msg' => 'Empty Password');
			echo json_encode($result);
			exit();
		}

		if($password['new'] != $password['repeat']) {
			$result = array('error' => 1, 'msg' => 'Passwords don\'t match');
			echo json_encode($result);
			exit();
		}
		if(!$login->change_password($user->email,$password['new'])) {
			$result = array('error' => 1, 'msg' => $login->errors[0]);
		}

		echo json_encode($result);
	}

	/**
	 * Method to autoregister a new user, calls Create function
	 *
	 */
	public function register() {
		ob_start();
		$this->create();

		$result = ob_get_clean();
		if (ob_get_length()) ob_end_clean();

		$result = json_decode($result);
		if($result->error) {
			Session::set('flashError','danger');
			if(!@$_POST['password']) {
				$result->msg .= 'Password required.';
			}
			Session::set('flash',$result->msg);
			Html::redirect_to(URL.'login/register');
			die();
		}
		$login = new Login();
		if(!$login->change_password($result->data->email,$_POST['password'])) {
			Session::set('flashError','danger');
			Session::set('flash','General Error');
			Html::redirect_to(URL.'login/register');
			die();
		}
		$mail = new Mail_Welcome($user);
		$mail->send();

		Html::redirect_to(URL.'login/register_ok');
	}

	/**
	 * Method to activate a user from a email link, hash used in creation has a one time only use.
	 *
	 */
	public function activation_link($email = '', $hash = '') {
		$user = Model::factory('Users')->where('email',$email)->where('hash',$hash)->find_one();
		if(!$user || !$email || !$hash) {
			$this->app->_error();
		}
		$user->hash = '';		//Hash has a one time only use.
		if($user->status == 'P') { //Only pending users can be activated this way
			$user->status = 'A';
		}
		$user->save();
		if($user->status == 'D') {
			Session::set('flashError','error');
			Session::set('flash','Your user has been disabled, please contact administrator.');
		} else {
			Session::set('flashError','success');
			Session::set('flash','Your activation has been successfully.<br>You can now login.');
		}
		Html::redirect_to(URL.'login');
	}

}