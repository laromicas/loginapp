<?php
	/*
	*	Auth Controller using PHP Facebook Auth
	*/
class Oauth_Controller extends Controller {

	function __construct() {
		parent::__construct();

		$this->socialFields = array("identifier"=>"id","profileURL"=>"link","photoURL"=>"image","displayName"=>"name","firstName"=>"first_name","lastName"=>"last_name","gender"=>"gender","language"=>"locale","email"=>"email");
		$this->userFields = array('phone','gender');

		require LIB_PATH.'Facebook/autoload.php';
		$this->login = new Login();
	}

	function index() {
		exit;
	}

	/**
	 * Receives callback of facebook
	 *
	 */
	function callback()
	{

		/* START OF FACEBOOK SDK */
		$fb = new Facebook\Facebook([
			'app_id' => FACEBOOK_APP_KEY, // Replace {app-id} with your app id
			'app_secret' => FACEBOOK_SECRET,
			'default_graph_version' => 'v2.2',
			]);

		$helper = $fb->getRedirectLoginHelper();

		try {
			$accessToken = $helper->getAccessToken();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			// When Graph returns an error
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			// When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}

		if (! isset($accessToken)) {
			if ($helper->getError()) {
				header('HTTP/1.0 401 Unauthorized');
				echo "Error: " . $helper->getError() . "\n";
				echo "Error Code: " . $helper->getErrorCode() . "\n";
				echo "Error Reason: " . $helper->getErrorReason() . "\n";
				echo "Error Description: " . $helper->getErrorDescription() . "\n";
			} else {
				header('HTTP/1.0 400 Bad Request');
				echo 'Bad request';
			}
			exit;
		}

		// // Logged in
		// echo '<h3>Access Token</h3>';
		// var_dump($accessToken->getValue());

		// The OAuth 2.0 client handler helps us manage access tokens
		$oAuth2Client = $fb->getOAuth2Client();

		// Get the access token metadata from /debug_token
		$tokenMetadata = $oAuth2Client->debugToken($accessToken);
		// echo '<h3>Metadata</h3>';
		// var_dump($tokenMetadata);

		// Validation (these will throw FacebookSDKException's when they fail)
		$tokenMetadata->validateAppId(FACEBOOK_APP_KEY); // Replace {app-id} with your app id
		// If you know the user ID this access token belongs to, you can validate it here
		//$tokenMetadata->validateUserId('123');
		$tokenMetadata->validateExpiration();

		if (! $accessToken->isLongLived()) {
			// Exchanges a short-lived access token for a long-lived one
			try {
				$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
			} catch (Facebook\Exceptions\FacebookSDKException $e) {
				echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
				exit;
			}
			// echo '<h3>Long-lived</h3>';
			// var_dump($accessToken->getValue());
		}

		$_SESSION['fb_access_token'] = (string) $accessToken;

		try {
			// Returns a `Facebook\FacebookResponse` object
			$response = $fb->get('me?fields=id,name,first_name,last_name,link,website,gender,locale,about,email,hometown,verified,birthday', $accessToken);
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
		$user = $response->getGraphUser();
		$photoSize = 250;

		/* END OF FACEBOOK SDK */

		//Get all social data fields
		$this->socialdata = array();
		foreach ($this->socialFields as $key => $value) {
			$this->socialdata[$key] = $user->getField($value);
		}
		$this->socialdata['photoURL'] = 'https://graph.facebook.com/v2.8/'.$user->getId().'/picture?width='.$photoSize.'&height='.$photoSize;
		$this->socialdata['provider'] = 'Facebook';
		$this->socialdata['verified'] = $user->getField('verified');

		// var_dump($this->socialdata);die();

		//Checks if social data exists
		$social = Model::factory('UsersSocials')->where('identifier',$this->socialdata['identifier'])->find_one();
		//If exists, finds users and logs him
		if($social) {
			$user = $social->User();
			//If not exists, update social, and creates user
			if($user) {
				// $this->getProfileImage($user,$social);
				$this->login($user,$social);
			} else {
				$user = $this->createUserFromSocial($social);
				$this->getProfileImage($user,$social);
				$this->login($user,$social);
			}
		} else {
			$social = $this->createSocialData($this->socialdata);
			$user = $this->createUserFromSocial($social);
			$this->getProfileImage($user,$social);
			$this->login($user,$social);
		}
	}

	/**
	 * Login user from social data
	 *
	 */
	private function login($user,$social) {
		if($this->login->social_login($social->email, $social->identifier)) {
			$user->set_expr('last_connect','NOW()');
			if($user->status == 'P') {
				$user->status = 'A';
			}
			$user->save();

			if(Session::get('URI')) {
				Html::redirect_to(URL);
				Html::redirect_to(Session::get('URI'));
				Session::delete('URI');
			} else {
				Html::redirect_to(URL);
			}
		} else {
			if(count($this->login->errors)) {
				$msg = $this->login->errors[0];
			} else {
				$msg = 'Wrong username or password';
			}
			Session::set('flashError','danger');
			Session::set('flash',$msg);
			Html::redirect_to(URL.'login');
		}
	}

	/**
	 * Creates user from social data
	 *
	 */
	private function createUserFromSocial($social) {
		if($user = Model::factory('Users')->where('email',$social->email)->find_one()) { //If there is any email same as social
			$social->user_id = $user->user_id;
			$social->save();
			if($user->status == 'P') {
				$user->status = 'A';
			}
			$user->save();
			return $user;
		}
		$user = Model::factory('Users')->create();
		$user->first_name = $social->firstName;
		$user->last_name = $social->lastName;
		$user->email = $social->email;
		$user->type = 'C';
		$user->status = 'A'; // Default Status -> Active, as email is already verified with facebook.
		$user->save();

		$social->user_id = $user->user_id;
		$social->save();
		return $user;
	}

	/**
	 * Creates dataset of social data
	 *
	 */
	private function createSocialData($socialdata) {
		$social = Model::factory('UsersSocials')->create();
		foreach ($socialdata as $key => $value) {
			$social->{$key} = $value;
		}
		$social->save();
		return $social;
	}

	/**
	 * Imports profile Image from Facebook
	 *
	 */
	private function getProfileImage($user,$social) {
		$image = file_get_contents($social->photoURL);
		$path = $user->getPath();
		file_put_contents($path.'profile-Facebook.jpg', $image);
	}
}