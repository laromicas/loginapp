<?php

/**
 * Controller Login
 * Handles the user's login, logout, to the view...
 * 
 */

class Login_Controller extends Controller {
	function __construct() {
		parent::__construct();
		// $this->view->handleLogin();
		$this->view->pageConfig['breadCrumbs'] = array();
		$this->view->pageConfig['sideMenu'] = array();
		require LIB_PATH.'Facebook/autoload.php';
		$this->login = new Login();
	}

	/**
	 * Shows login page
	 *
	 */

	function index() {
		require ROOT_PATH.'config/sidemenu.php';
		$this->view->sideMenu = $GLOBALS['sidemenu'];


		$fb = new \Facebook\Facebook([
			'app_id' => FACEBOOK_APP_KEY, // Replace {app-id} with your app id
			'app_secret' => FACEBOOK_SECRET,
			'default_graph_version' => 'v2.2',
			'auth_type' => 'reauthenticate'
			]);

		$helper = $fb->getRedirectLoginHelper();
		$permissions = ['email', 'public_profile', 'user_friends']; // Optional permissions
		$this->view->loginUrl = $helper->getLoginUrl(Html::protocol().$_SERVER["HTTP_HOST"].URL.'oauth/callback', $permissions);

		$this->view->render('login',array('headerfooter'=>false));
	}

	/**
	 * Shows register new page
	 *
	 */
	function register() {
		$fb = new \Facebook\Facebook([
			'app_id' => FACEBOOK_APP_KEY, // Replace {app-id} with your app id
			'app_secret' => FACEBOOK_SECRET,
			'default_graph_version' => 'v2.2',
			'auth_type' => 'reauthenticate'
			]);

		$helper = $fb->getRedirectLoginHelper();
		$permissions = ['email', 'public_profile', 'user_friends']; // Optional permissions
		$this->view->loginUrl = $helper->getLoginUrl(Html::protocol().$_SERVER["HTTP_HOST"].URL.'oauth/callback', $permissions);

		$this->view->render('register',array('headerfooter'=>false));
	}

	/**
	 * Shows register new page
	 *
	 */
	function register_ok() {
		$fb = new \Facebook\Facebook([
			'app_id' => FACEBOOK_APP_KEY, // Replace {app-id} with your app id
			'app_secret' => FACEBOOK_SECRET,
			'default_graph_version' => 'v2.2',
			'auth_type' => 'reauthenticate'
			]);

		$helper = $fb->getRedirectLoginHelper();
		$permissions = ['email', 'public_profile', 'user_friends']; // Optional permissions
		$this->view->loginUrl = $helper->getLoginUrl(Html::protocol().$_SERVER["HTTP_HOST"].URL.'oauth/callback', $permissions);

		$this->view->render('register_ok',array('headerfooter'=>false));
	}

	/**
	 * Executes login process and shows error or redirects to homepage
	 *
	 */
	function login() {
		$username = (@$_POST['email']) ? $_POST['email'] : @$_POST['username'];
		$password = (@$_POST['password']) ? $_POST['password'] : @$_POST['login']['password'];
		$login_successful = $this->login->login($username,$password);
		$this->view->errors = $this->login->errors;
		if ($login_successful) {
			$user = Session::get('user_data');
			if($user->status != 'A') {
				Session::set('flashError','danger');
				Session::set('flash','User disabled, please contact administrator.');
				Html::redirect_to(URL.'login');
			} else {
				$user->set_expr('last_connect','NOW()');
				$user->save();

				if(Session::get('URI')) {
					Html::redirect_to(URL);
					Html::redirect_to(Session::get('URI'));
					Session::delete('URI');
				} else {
					Html::redirect_to(URL);
				}
			}
		} else {
			$msg = (count($this->login->errors)) ? $this->login->errors[0] : 'Wrong username or password';
			Session::set('flashError','danger');
			Session::set('flash', $msg);
			Html::redirect_to(URL.'login');
		}
	}


	/**
	 * Executes logout process
	 *
	 */
	function logout() {
		$this->login->logout();
		Html::redirect_to(URL.'login');
	}
}