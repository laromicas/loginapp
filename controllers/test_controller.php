<?php

class Test_Controller extends Controller {
	function __construct() {
		parent::__construct();
		if(!TESTING) {
			Html::redirect_to(URL.'error');
			die();
		}
	}

	function index() {

	}

	/**
	 * Formats Green OK Text
	 *
	 * @param string $test Text to show, defaul OK
	 */
	function ok($text = false) {
		$text = ($text) ? $text : 'OK';
		return '<span style="color:green">'.$text.'</span><br>';
	}

	/**
	 * Formats Red Error Text
	 *
	 * @param string $test Text to show, defaul Error
	 */
	function error($text = false) {
		$text = ($text) ? $text : 'Error';
		return '<span style="color:red">'.$text.'</span><br>';
	}

	/**
	 * Formats Warning Not Tested Text
	 *
	 * @param string $test Text to show, defaul Not Tested
	 */
	function warning($text = false) {
		$text = ($text) ? $text : 'Not Tested';
		return '<span style="color:orange">'.$text.'</span><br>';
	}

	/**
	 * Test login and logout process
	 *
	 * @param string $test Text to show, defaul Not Tested
	 */
	function test_login() {
		// echo password_hash('password',PASSWORD_BCRYPT);
		echo 'Test login OK:';
		$login = new Login();
		if($login->login('lacides@gmail.com','password')) {
			if(count($login->errors)) {
				echo $this->error();
			} else {
				echo $this->ok();
			}
		} else {
			echo $this->error();
		}

		echo 'Test wrong password:';
		$login = new Login();
		if(!$login->login('lacides@gmail.com','password1')) {
			if(count($login->errors) && $login->errors[0] == 'Wrong Password') {
				echo $this->ok();
			} else {
				echo $this->error();
			}
		} else {
			echo $this->error();
		}

		echo 'Test wrong username:';
		$login = new Login();
		if(!$login->login('lalala@hotmail.com','password1')) {
			if(count($login->errors) && $login->errors[0] == 'User doesn\'t exists') {
				echo $this->ok();
			} else {
				echo $this->error();
			}
		} else {
			echo $this->error();
		}

		echo 'Test empty username:';
		$login = new Login();
		if(!$login->login('','password')) {
			if(count($login->errors) && $login->errors[0] == 'Empty username') {
				echo $this->ok();
			} else {
				echo $this->error();
			}
		} else {
			echo $this->error();
		}

		echo 'Test empty password:';
		$login = new Login();
		if(!$login->login('lacides@gmail.com','')) {
			if(count($login->errors) && $login->errors[0] == 'Empty password') {
				echo $this->ok();
			} else {
				echo $this->error();
			}
		} else {
			echo $this->error();
		}

		echo 'Test logout:';
		$login = new Login();
		$login->logout();
		if(!Session::get('user_data') && !Session::get('user_logged_in')) {
			echo $this->ok();
		} else {
			echo $this->error();
		}
	}

	/**
	 * Test users CRUD methods
	 *
	 * @param string $user Test CRUD methods against $user, password must be 'password'
	 */
	function test_users($user = false) {

		$useragent = $_SERVER['HTTP_USER_AGENT'];
		$strCookie = 'PHPSESSID=' . $_COOKIE['PHPSESSID'] . '; path=/';
		if($user) {
			session_write_close();
			$curl = curl_init();
			$data = array('email'=>$user,'password'=>'password');
			curl_setopt($curl, CURLOPT_URL, $_SERVER["HTTP_HOST"].URL.'login/login');
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
			curl_setopt($curl,CURLOPT_USERAGENT, $useragent);
			curl_setopt($curl, CURLOPT_COOKIE, $strCookie);
			$result = curl_exec($curl);
			echo $result;
			curl_close($curl);
		}

		echo 'Test List Users:';
		session_write_close();
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $_SERVER["HTTP_HOST"].URL.'users');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl,CURLOPT_USERAGENT, $useragent);
		curl_setopt($curl, CURLOPT_COOKIE, $strCookie);

		$result = curl_exec($curl);
		if(count(json_decode($result))) {
			echo $this->ok();
		} else {
			echo $this->error();
		}

		echo 'Test Create User:';
		session_write_close();
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $_SERVER["HTTP_HOST"].URL.'users');
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl,CURLOPT_USERAGENT, $useragent);
		curl_setopt($curl, CURLOPT_COOKIE, $strCookie);

		curl_setopt($curl, CURLOPT_POSTFIELDS, 'first_name=Test&last_name=Last&email=test@test.com&password=test');
		$result = curl_exec($curl);
		$result = json_decode($result);
		curl_close($curl);
		if($result && $result->error == 0 || @$result->msg == 'E-Mail already registered') {
			$user = Model::factory('Users')->where('email','test@test.com')->find_one();
			if($user) {
				echo $this->ok();
			} else {
				echo $this->error();
			}
		} else {
			echo $this->error();
		}

		echo 'Test Read User:';
		if(@$user) {
			$user_id = $user->user_id;
			$user_name = $user->first_name;
		} else {
			$user_id = 1;
			$user_name = 'System';
		}
		session_write_close();
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $_SERVER["HTTP_HOST"].URL.'users/'.$user_id);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl,CURLOPT_USERAGENT, $useragent);
		curl_setopt($curl, CURLOPT_COOKIE, $strCookie);

		$result = curl_exec($curl);
		$user = json_decode($result);
		curl_close($curl);
		if($user && $user->first_name == $user_name) {
			echo $this->ok();
		} else {
			echo $this->error();
		}
		do {
			echo 'Test Update User:';
			if($user_id == '1') {
				$this->warning();
				break;
			}

			$data = array('first_name'=>'Test1','last_name'=>'Last1','phone'=>'3115550101','email'=>'test@test.com');
			session_write_close();
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $_SERVER["HTTP_HOST"].URL.'users/'.$user_id);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl,CURLOPT_USERAGENT, $useragent);
			curl_setopt($curl, CURLOPT_COOKIE, $strCookie);
 
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
			$result = curl_exec($curl);
			$result = json_decode($result);
			// var_dump($result);
			curl_close($curl);
			if($result && $result->error == 0) {
				sleep(1);
				$users = Model::factory('Users')->where('email','test@test.com')->find_array();
				$user = $users[0];
				if($user['phone'] == '3115550101') {
					echo $this->ok();
				} else {
					echo $this->error();
				}
			} else {
				echo $this->error();
			}
		} while(0);

		do {
			echo 'Test Delete User:';
			if($user_id == '1') {
				$this->warning();
				break;
			}
			session_write_close();
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $_SERVER["HTTP_HOST"].URL.'users/'.$user_id);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl,CURLOPT_USERAGENT, $useragent);
			curl_setopt($curl, CURLOPT_COOKIE, $strCookie);
 
			$result = json_decode(curl_exec($curl));
			curl_close($curl);
			if($result && $result->error == 0) {
				$users = Model::factory('Users')->where_id_is($user_id)->find_array();
				if(!count($users)) {
					echo $this->ok();
				} else {
					echo $this->error();
				}
			} else {
				echo $this->error();
			}
		} while (0);
	}

	/**
	 * Test users CRUD methods
	 *
	 * @param string $user Test CRUD methods against Admin, Agent and Customer test users'
	 */
	function all_users_test() {
		echo 'Testing Admin:<br>Must be: OK, OK, OK, OK, OK<br>';
		$this->test_users('admin@test.com');
		echo 'Testing Agent:<br>Must be: OK, OK, Error, Error, Error<br>';
		$this->test_users('agent@test.com');
		echo 'Testing Customer:<br>Must be: Error, OK, Error, Error, Error<br>';
		$this->test_users('customer@test.com');
	}
}