<?php
/*
*	Files Controller handles excel files
*/
class Files_Controller extends Controller {

	function __construct() {
		parent::__construct();
		$this->view->handleLogin();

		$this->allowedExcelTypes = array("application/vnd.ms-excel",
							"application/msexcel",
							"application/x-msexcel",
							"application/x-ms-excel",
							"application/x-excel",
							"application/x-dos_ms_excel",
							"application/xls",
							"application/x-xls",
							"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
							"text/plain",
							"text/csv");

	}

	/*
	*	Get excel file of users
	*/
	public function users() {
		if(@$_GET['file'] and in_array(@$_GET['file'],array('xls','xlsx','csv','tsv'))) {
			$excel = new Excel($_GET['file']);
			$excel->getUsers();
			$excel->output();
		}
	}

	/*
	*	Process import of excel file
	*/
	public function upload_users() {
		// var_dump($_POST);var_dump($_FILES);die();
		$result = array('error'=>0);
		if(!@$_FILES['users']['name']) {
			$result = array('error'=>1, 'msg'=>'Error en el archivo');
			echo json_encode($result);
			return;
		}
		$temp = explode(".", $_FILES["users"]["name"]);
		$inputFileName = $_FILES["users"]["tmp_name"];

		$extension = end($temp);
		if($extension == 'xls' && !$_FILES['users']['type']) { $_FILES['users']['type'] = "application/vnd.ms-excel"; }
		if($extension == 'xlsx' && !$_FILES['users']['type']) { $_FILES['users']['type'] = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"; }
		if($extension == 'tsv') { $_FILES['users']['type'] = "text/csv"; }
		if($extension == 'csv') { $_FILES['users']['type'] = "text/csv"; }
		if(!in_array($_FILES['users']['type'], $this->allowedExcelTypes)) {
			$result = array('error'=>1, 'msg'=>'Tipo de archivo no permitido '.$_FILES['users']['type']);
			echo json_encode($result);
			return;
		}

		$excel = new Excel();
		$errores = $excel->importUsersExcel($inputFileName, $extension);
		$result = array("error" => 0);
		if(sizeof($errores) > 1) {
			$errorFileName = 'errores-'.date('YmdHi');
			$fileErrores = $excel->saveErrors($errores,TMP_PATH.$errorFileName,$extension);

			$result = array('error'=>1, 'msg'=>'Errors importing click en link to check','file'=>$fileErrores);
			echo json_encode($result);
			return;
		} else {
			echo json_encode($result);
			return;
		}
	}
}