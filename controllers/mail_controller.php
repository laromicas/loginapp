<?php 
	/*
	*	Shows preview versions of Mail (if valid)
	*/
class Mail_Controller extends Controller {
	function welcome($email = '', $hash = '') {
		if(!$email) { $email = 'lacides@gmail.com'; $hash = 'f55e25fe8b8b977b5c4f26c7a94ccab921760d59f727a45f122b0e93f2a61fed'; }
		
		$user = Model::factory('Users')->where('email',$email)->where('hash',$hash)->find_one();
		if(!$user || !$email || !$hash) {
			$this->app->_error();
		}
		$mail = new Mail_Welcome($user);
		echo $mail->get_html();
		if(@$_GET['send']) {
			if($mail->send()) {
				echo 'Elemento enviado';
			} else {
				$error=1;
				echo "Se ha producido un error al enviar el correo.";
				echo "Mailer Error: " . $mail->ErrorInfo;
				var_dump($mail->Mailer);
				exit;
			}
		}
	}	
}
