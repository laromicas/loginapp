<?php

class UsersSocials extends Model
{
	public static $_table = 'users_socials';
	public static $_id_column = 'users_socials_id';

	public function User() {
		return $this->belongs_to('Users', 'user_id')->find_one();
	}
}