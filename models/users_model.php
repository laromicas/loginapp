<?php

/**
 * Model Users
 * handles the connection to database for table users
 * 
 */

class Users extends Model
{
	public static $_table = 'users';
	public static $_id_column = 'user_id';

	/**
	 * Returns full name
	 *
	 */
	public function getFullName() {
		return $this->first_name.' '.$this->last_name;
	}

	/**
	 * Returns full url to user files
	 *
	 */
	public function getURL() {
		return USERS_URL.$this->user_id.'/';
	}

	/**
	 * Returns path to user files
	 *
	 */
	public function getPath() {
		return USERS_PATH.$this->user_id.'/';
	}

	/**
	 * Return all user files
	 *
	 */
	public function getFiles() {
		if($this->image) { return $this->image; }
		$dir = $this->getPath();
		if(!is_dir($dir)) {
			mkdir($dir);
		}
		$this->files = scandir($dir);
		// var_dump($this->files);die();
		foreach ($this->files as $file) {
			if(in_array($file, array('.','..'))) { continue; }
			$ext = explode('.', $file);
			$extension = end($ext);

			if(in_array($extension, array('jpg','jpeg','png'))) {
				$this->profileImage = $file;
			}
		}
	}

	/**
	 * Returns full url of User Profile Image
	 *
	 */
	public function getProfileImage() {
		if($this->profileImage) { return $this->getURL().$this->profileImage; }
		$this->getFiles();
		if(file_exists($this->getPath().$this->profileImage) && !is_dir($this->getPath().$this->profileImage)) {
			return $this->getURL().$this->profileImage;
		} else {
			return URL.'assets/img/photo.png';
		}
	}

	/**
	 * Returns social if any
	 *
	 */
	public function Social($provider) {
		return $this->has_many('UsersSocials','user_id')->where('provider',$provider)->find_one();
	}
}