/***
Metronic AngularJS App Main Script
***/

/* Metronic App */
var ClickLoginApp = angular.module("ClickLoginApp", [
	"ui.router",
	"ui.bootstrap",
	"oc.lazyLoad",
	'ui.select',
	"ngSanitize",
	'daterangepicker',
	'easypiechart',
	// 'amChartsDirective',
	// 'countUpModule'
	'ngFileUpload',
	'datatables','datatables.buttons',
	// 'datatables.bootstrap',
	'chart.js',
	'ui.mask',
	'frapontillo.bootstrap-switch',
	// 'uiGmapgoogle-maps',
	// 'nvd3',
	'angular-ladda'
]);


/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
ClickLoginApp.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
	$ocLazyLoadProvider.config({
		// global configs go here
	});
}]);

//AngularJS v1.3.x workaround for old style controller declarition in HTML
ClickLoginApp.config(['$controllerProvider', function($controllerProvider) {
	// this option might be handy for migrating old apps, but please don't use it
	// in new ones!
	$controllerProvider.allowGlobals();
}]);

/********************************************
 END: BREAKING CHANGE in AngularJS v1.3.x:
*********************************************/

/* Setup global settings */
ClickLoginApp.factory('settings', ['$rootScope', function($rootScope) {
	// supported languages
	var settings = {
		layout: {
			pageSidebarClosed: false, // sidebar menu state
			pageContentWhite: true, // set page content layout
			pageBodySolid: false, // solid body color state
			pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
		},
		assetsPath: 'assets',
		globalPath: 'assets/global',
		layoutPath: 'assets/layouts/layout2',
		easyChartOptions: {
			barColor: function (percent) {
				return (percent < 85 ? '#ff5f5f' : percent < 95 ? '#ffbb33' : '#99cc00' );
			},
			scaleColor: false,
			lineWidth: 5,
			lineCap:'circle'
		}
	};

	$rootScope.settings = settings;

	return settings;
}]);

/* Setup App Main Controller */
ClickLoginApp.controller('AppController', ['$scope', '$rootScope', '$uibModal', 'clients', 'dispatchEdit', function($scope, $rootScope, $uibModal, clients, dispatchEdit) {
	$scope.$on('$viewContentLoaded', function() {
		//App.initComponents(); // init core components
		//Layout.init(); //	Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive
	});
}]);

/* Setup Layout Part - Header */
ClickLoginApp.controller('HeaderController', ['$scope', function($scope) {
	$scope.$on('$includeContentLoaded', function() {
		// Layout.initHeader(); // init header
	});
}]);

/* Setup Layout Part - Sidebar */
ClickLoginApp.controller('SidebarController', ['$state', '$scope', function($state, $scope) {
	// console.log($scope);
	$scope.setActive = function(event) {
		$('.page-sidebar ul li').removeClass('active');
		$('.page-sidebar ul li span.selected').remove();
		$(event.currentTarget).closest('li').addClass('active');
		$(event.currentTarget).append('<span class="selected"></span>');
	}
	$scope.$on('$includeContentLoaded', function() {
		// Layout.initSidebar($state); // init sidebar
	});
}]);

/* Setup Layout Part - Quick Sidebar */
ClickLoginApp.controller('QuickSidebarController', ['$scope', function($scope) {
	$scope.$on('$includeContentLoaded', function() {
		 // setTimeout(function(){
		 //		QuickSidebar.init(); // init quick sidebar
		 //	}, 2000)
	});
}]);

/* Setup Layout Part - Theme Panel */
ClickLoginApp.controller('ThemePanelController', ['$scope', function($scope) {
	$scope.$on('$includeContentLoaded', function() {
		// Demo.init(); // init theme panel
	});
}]);

/* Setup Layout Part - Footer */
ClickLoginApp.controller('FooterController', ['$scope', function($scope) {
	$scope.$on('$includeContentLoaded', function() {
		// Layout.initFooter(); // init footer
	});
}]);

/* Setup Rounting For All Pages */
ClickLoginApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

	(function (ChartJsProvider) {
		ChartJsProvider.setOptions({ colors : [ '#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'] });
	});


	// Redirect any unmatched url
	$urlRouterProvider.otherwise("/"); 

	$stateProvider
		// Dashboard
		.state('dashboard', {
			url: "/",
			templateUrl: "views/angular/user_edit.html",
			data: {pageTitle: 'Dashboard'},
			controller: "UserEditController",
			resolve: {
				deps: ['$ocLazyLoad', function($ocLazyLoad) {
					return $ocLazyLoad.load([{
						name: 'angularFileUpload',
						insertBefore: '#ng_load_plugins_before',
						files: [
							fixurl+'assets/pages/css/profile.min.css',
							fixurl+'assets/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
						]
					}, {
						name: 'CosmoReportApp',
						files: [
							'angular/controllers/UserEditController.js'
						]
					}]);
				}]
			}
		})
		.state('users', {
			url: "/users",
			templateUrl: "views/angular/users.html",
			data: {pageTitle: 'Users'},
			controller: "UsersController",
			resolve: {
				deps: ['$ocLazyLoad', function($ocLazyLoad) {
					return $ocLazyLoad.load({
						name: 'CosmoReportApp',
						insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
						files: [
							// fixurl+'assets/pages/scripts/table-datatables-managed.min.js',

							// 'assets/pages/scripts/users.min.js',
							'angular/controllers/UsersController.js',
						]
					});
				}]
			}
		})
		.state('userEdit', {
			url: '/user/:id',
			templateUrl: "views/angular/user_edit.html",
			data: {pageTitle: 'Edit User'},
			controller: "UserEditController",
			resolve: {
				deps: ['$ocLazyLoad', function($ocLazyLoad) {
					return $ocLazyLoad.load([{
						name: 'angularFileUpload',
						insertBefore: '#ng_load_plugins_before',
						files: [
							fixurl+'assets/pages/css/profile.min.css',
							fixurl+'assets/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
						]
					}, {
						name: 'CosmoReportApp',
						files: [
							'angular/controllers/UserEditController.js'
						]
					}]);
				}]
			}
		})
}]);

/* Init global settings and run the app */
ClickLoginApp.run(["$rootScope", "settings", "$state", function($rootScope, settings, $state) {
	$rootScope.$state = $state; // state to be accessed from view
	$rootScope.$settings = settings; // state to be accessed from view
}]);


ClickLoginApp.controller('UserController', function($scope, userData) {
	// console.log($scope);
	userData.then(function(data) {
		$scope.userData = data;
	});
});

ClickLoginApp.controller('NotificationController', function($scope, $interval, getData) {
	$scope.notifications = [];
	$interval(function () {
		getData.getNotifications().then(function (data) {
			$scope.notifications = data;
		});
	}, 10000);

});


function getMonday(d) {
	d = new Date(d);
	var day = d.getDay(),
		diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
	return new Date(d.setDate(diff));
}

function getSunday(d) {
	d = new Date(d);
	var day = d.getDay(),
		diff = d.getDate() - day + (day == 0 ? 0:7); // adjust when day is sunday
	return new Date(d.setDate(diff));
}

function shuffle(a) {
		var j, x, i;
		for (i = a.length; i; i--) {
				j = Math.floor(Math.random() * i);
				x = a[i - 1];
				a[i - 1] = a[j];
				a[j] = x;
		}
		return a;
}

Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf())
    date.setDate(date.getDate() + days);
    return date;
}

function getDates(startDate, stopDate) {
	startDate = moment(startDate)._d;
	stopDate = moment(stopDate)._d;
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push(new Date (currentDate));
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}