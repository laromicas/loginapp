ClickLoginApp.factory('userData', ['$http', function($http) { 
	return $http.get(fixurl+'users/current') 
		.then(function(data) {
			toastr["success"]('Good to see you, '+data.data.first_name+'!');
			return data.data; 
		})
		// .error(function(err) { 
		// 	return err; 
		// });
}]);

ClickLoginApp.factory('allusers', ['$http', function($http) {
	return $http.get(fixurl+'users');
}]);

ClickLoginApp.filter('phoneLink', function() {
	return function(input, icon) {
		input = input || '';
		input = input.trim();
		var out = '';
		if(input.length == 10) {
			out = '(';
			out += input.slice(0, 3);
			out += ') ';
			out += input.slice(3, 6);
			out += '-';
			out += input.slice(6);
			if(icon) {
				if(icon.length) {
					return '<a href="tel:'+input+'"><i class="'+icon+'"></i>'+out+'</a>';
				}
				return '<a href="tel:'+input+'"><i class="fa fa-phone"></i>'+out+'</a>';
			}
			return '<a href="tel:'+input+'">'+out+'</a>';
		} else {
			return input;
		}
	};
});

ClickLoginApp.filter('moment', function() {
	return function(input,format) {
		if(!input) {
			return '';
		}
		var date = moment(input);
		if(format) {
			newdate = date.format(format);
			return (newdate == 'Invalid date') ? '' : newdate;
		}
		return date.format();
	};
})

/**
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs an AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform an OR.
 */
ClickLoginApp.filter('propsFilter', function() {
	return function(items, props) {
		var out = [];

		if (angular.isArray(items)) {
			var keys = Object.keys(props);

			items.forEach(function(item) {
				var itemMatches = false;

				for (var i = 0; i < keys.length; i++) {
					var prop = keys[i];
					var text = props[prop].toLowerCase();
					if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
						itemMatches = true;
						break;
					}
				}

				if (itemMatches) {
					out.push(item);
				}
			});
		} else {
			// Let the output be the input untouched
			out = items;
		}

		return out;
	};
});
