angular.module('ClickLoginApp').controller('UsersController', function($rootScope, $scope, $state, $http, $timeout, $uibModal, Upload, userData) {
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        App.initAjax();
    });

    console.log($scope);

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;

	$scope.editUsers = false;
	$scope.listUsers = false;

	userData.then(function(data) {
		$scope.userData = data;
		$scope.editUsers = ($scope.userData.type == 'A');
		$scope.listUsers = ($scope.userData.type == 'A' || $scope.userData.type == 'G');

		if(!$scope.editUsers && !$scope.listUsers) {
			// $state.go('dashboard');
		}
	});

	$scope.users = [];
	$scope.statuses = {
		A:'Active',
		P:'Pending Activation',
		D:'Disabled'
	};

	$scope.types = {
		A:'Admin',
		G:'Agent',
		C:'Client'
	};


	$scope.getData = function () {
		send_data = {};

		$http.get(fixurl+'users',{params:send_data}) 
			.then(function(data) {
				$scope.users = data.data;
			});
	}
	$scope.getData();

	$scope.deleteUser = function (index) {
		if(confirm('Are you sure to delete this user? This cannot be undone.')) {
			user = $scope.users[index];
			Upload.upload({
				url: fixurl+'users/'+user.user_id,
				data: {user_id: user.user_id, method:'_delete'}
			}).then(function (data) {
				// console.log('Success ' + data.config.data.image.name + 'uploaded. Response: ' + data.data);
				var message = data.data.msg;
				swal({
					title: (data.data.error) ? "Error" : "Success",
					text: message,
					type: (data.data.error) ? "error" : "success",
					// showCancelButton: true,
					confirmButtonClass: 'btn-success',
					confirmButtonText: 'OK'
				});
				if(!data.data.error) {
					$scope.users.splice(index,1);
				}

			}, function (data) {
				// console.log('Error status: ' + data.status);
			}, function (evt) {
				var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				// console.log('progress: ' + progressPercentage + '% ' + evt.config.data.image.name);
			});
		}
	}
	$scope.newUploadFile = false;

	$scope.showButtonUpload = false;
	$scope.showUpload = function () {
		$scope.showButtonUpload = true;
	}

	$scope.fileUpdated = function ($files, $event) {
		$scope.newUploadFile = $event.target.files[0];
	}

	$scope.upload = function () {
		Upload.upload({
			url: fixurl+'files/upload_users',
			data: {users: $scope.newUploadFile}
		}).then(function (data) {
			// console.log('Success ' + data.config.data.image.name + 'uploaded. Response: ' + data.data);
			var message = (data.data.error) ? data.data.msg : 'File imported successfully';
			if(data.data.file) {
				message+=' <a href="'+fixurl+'tmp/'+data.data.file+'">Errors</a>'
			};

			swal({
				title: (data.data.error) ? "Error" : "Success",
				html: true,
				text: message,
				type: (data.data.error) ? "error" : "success",
				// showCancelButton: true,
				confirmButtonClass: 'btn-success',
				confirmButtonText: 'OK'
			});
		}, function (data) {
			// console.log('Error status: ' + data.status);
		}, function (evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			// console.log('progress: ' + progressPercentage + '% ' + evt.config.data.image.name);
		});
	}

	$scope.upload_file = function () {
		
	}
});