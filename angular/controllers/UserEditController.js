angular.module("ClickLoginApp").controller('UserEditController', function($scope, $stateParams, $http, $state, $timeout, userData, Upload) {
	// console.log($scope);

	$scope.userData = {};

	$scope.types = [
		{type:'A',name:'Admin',selected:false},
		{type:'G',name:'Agent',selected:false},
		{type:'C',name:'Client',selected:false}
	];

	$scope.statuses = [
		{status:'A',name:'Active'},
		{status:'P',name:'Pending Activation'},
		{status:'D',name:'Disabled'}
	];


	$scope.editUsers = false;
	$scope.id = $stateParams.id;

	userData.then(function(data) {
		$scope.userData = data;
		$scope.editUsers = ($scope.userData.type == 'A');
		if(!$scope.id) {
			$scope.id = $scope.userData.user_id;
		}
		if(!$scope.editUsers && $scope.userData.user_id != $scope.id) {
			// $state.go('dashboard');
		}

		$scope.newProfileImage = false;
		$scope.invalidFile = {};

		$('.content').hide();
		$('#loadingIcon').show();

		$scope.clientList = [];

		if($scope.id != 'new') {
			$http.get(fixurl+'users/'+$scope.id)
				.then(function(data) {
					$scope.user = data.data;
					$('.content').show();
					$('#loadingIcon').hide();

					$timeout(function () {
						var drEvent = $('.dropify').dropify();
						drEvent.on('dropify.afterClear', function(event, element){
							// $scope.newProfileImage = false;
							// $scope.$apply();
						});
						$scope.password = {};
					}, 500);
				});
		} else {
			$('.content').show();
			$('#loadingIcon').hide();
			$timeout(function () {
				var drEvent = $('.dropify').dropify();
				drEvent.on('dropify.afterClear', function(event, element){
					$scope.newProfileImage = false;
					$scope.$apply();
				});
				$scope.password = {};
			}, 500);
			$scope.user = {};
		}
	});


	$scope.password = {};

	$scope.submit = function() {
		if($scope.id == 'new') {
			url = fixurl+'users';
			method = '_post';
		} else {
			url = fixurl+'users/'+$scope.id;
			method = '_put';
		}
		var data = $scope.user;
		data['method'] = method;
		data['image'] = $scope.newProfileImage;

		Upload.upload({
			url: url,
			data: data
		}).then(function (data) {
			// console.log('Success ' + data.config.data.image.name + 'uploaded. Response: ' + data.data);
			var message = data.data.error_msg;
			if(typeof(data.data.error_msg) == 'object') {
				// console.log(data.data.error_msg);
				if(data.data.error_msg.errorInfo) {
					message = data.data.error_msg.errorInfo[2];
				}
			}

			swal({
				title: (data.data.error) ? "Error" : "Success",
				text: message,
				type: (data.data.error) ? "error" : "success",
				// showCancelButton: true,
				confirmButtonClass: 'btn-success',
				confirmButtonText: 'OK'
			});
			if(data.data.data.user_id) {
				$scope.user.user_id = data.data.data.user_id;
				$state.go('users');
			}

		}, function (data) {
			// console.log('Error status: ' + data.status);
		}, function (evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			// console.log('progress: ' + progressPercentage + '% ' + evt.config.data.image.name);
		});
	}

	$scope.savePassword = function() {
		Upload.upload({
			url: fixurl+'users/password/'+$scope.user.user_id,
			data: {user_id: $scope.user.user_id, password: $scope.password,method:'_put'}
		}).then(function (data) {
			swal({
				title: (data.data.error) ? "Error" : "Success",
				text: data.data.msg,
				type: (data.data.error) ? "error" : "success",
				confirmButtonClass: 'btn-success',
				confirmButtonText: 'OK'
			});
			$scope.password = {
				old: '',
				new: '',
				repeat: ''
			};
		});
	}

	$scope.isEmptyObject = function (item){
		return angular.equals( {}, item) ? true : angular.equals( [], item);
	}
});