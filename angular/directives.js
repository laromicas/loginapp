ClickLoginApp.directive('tableApp', [ 'DTOptionsBuilder', 'DTColumnBuilder', function(DTOptionsBuilder, DTColumnBuilder) {
	return {
		restrict: 'E', 
		scope: false,
		// templateUrl: fixurl+'angular/js/directives/company_table.html',
		template: '<ng-include src="getTemplateUrl()"/>',
		link: function(scope, element, attrs) {

		},
		controller:function($scope, $element, $attrs){
			$scope.getTemplateUrl = function() {
				return fixurl+$attrs.template;
			}
			$scope.dtOptions = {
				// "dom": '<"toolbar tool"><"clear-filter">frtip',
				info: false,
				paging: false,
				responsive: true,
				"oLanguage": { "sSearch": "" },
				order: [[1, 'desc'],[1, 'asc']]
			}

			var buttons = [
				{
					text: 'Excel',
					key: '1',
					className: 'btn',
					action: function (e, dt, node, config) {
						$scope.getExcel();
					}
				}
			];

			$scope.dtOptions = DTOptionsBuilder.newOptions()
				// .withDOM('<"toolbar tool"><"clear-filter">fBrtip')
				.withDOM("<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>")
				
				// .withDOM('frtip')
				.withOption('info', false)
				.withOption('paging', false)
				.withOption('responsive', true)
				.withOption('oLanguage',{ "sSearch": '<a class="btn searchBtn" id="searchBtn"><i class="fal fa-search"></i></a>' })
				.withOption('order',[[1, 'desc'],[0, 'asc']])
				.withOption('drawCallback', function(settings) {
					// $("div.tool").html('<h5 class="zero-m">'+$attrs.name+'</h5>');
					// $("div.dataTables_filter input").attr('placeholder', 'Search');
					// $('#searchBtn').next('input').val('office:TR')
				})
				// .withButtons(buttons);
		}
	};
}]);