# Clickdelivery Login Test Apps's README

This app is a Login App and User Management.

[Clickdelivery Test App](http://54.187.88.30/loginapp/)

[Source Code](https://bitbucket.org/laromicas/loginapp.git) Bitbucket Public Repository

Current Users to test:

- Administrator
	- user: *admin@test.com*
	- password: *password*
- Agent
	- user: *agent@test.com*
	- password: *password*
- Client
	- user: *client@test.com*
	- password: *password*

<!-- MarkdownTOC autolink="true" bracket="round" markdown_preview="markdown" -->

- [Scripts Specific to this App](#scripts-specific-to-this-app)
	- [Model](#model)
		- [Database](#database)
		- [Model Scripts](#model-scripts)
	- [View](#view)
		- [HTML files](#html-files)
		- [PHP View files](#php-view-files)
	- [Controller](#controller)
		- [Javascript](#javascript)
		- [PHP](#php)
			- [Libraries](#libraries)
			- [Controllers](#controllers)
	- [Testing](#testing)
- [Framework](#framework)
	- [Frontend](#frontend)
		- [PHP Framework Files](#php-framework-files)
		- [HTML and Javascript Framework](#html-and-javascript-framework)
	- [Backend](#backend)
		- [External Libraries](#external-libraries)

<!-- /MarkdownTOC -->

## Scripts Specific to this App

### Model

#### Database
> - [Relation Database](http://54.187.88.30/loginapp/dml.png) Database
> - [Online Relation Database](https://repository.genmymodel.com/laromicas/LoginApp)
> - [DML](http://54.187.88.30/loginapp/database.sql)
> - Tables
> - **users** - Users Table with basic data
> - **users_socials** - Table with Social data of the users
> 
---

#### Model Scripts
> 	- **users_model.php** - Model for Users Table
> 	- **userssocials_model.php** - Model for Users_Socials Table, Facebook data
>
---

### View

#### HTML files
> - **views/angular/users.html** - List Users, use UsersController.js controller
> - **views/angular/users_table.html** - Table to show users inside users.html
> - **views/angular/users_edit.html** - Edit users, uses UserEditController.js controller
>
---

#### PHP View files
> - **views/login.php** - Login Page
> - **views/index.php** - App Page
> - **views/register.php** - Register Page
> - **views/register_ok.php** - Register Confirmation Page
> - **libs/Mail/template.php** - E-mail HTML Template
> - **libs/Mail/template_register.php** - E-mail HTML Template for new registrations
>
---

### Controller

#### Javascript
> - **angular/main.js** - angular definitions and main controller
> - **angular/services.js** - services to read data from backend
> - **angular/directives.js** - angular directives
> - **angular/controllers/\*** - Controllers and inner functions to show, prepare, send and receive data
> - **angular/controllers/UsersController.js** - Users List and Delete View Controller
> - **angular/controllers/UserEditController.js** - User View and Edit View Controller
>
---

#### PHP
##### Libraries
> - **libs/Login.php** - Handles the user's login, logout, password changing
> 	- *functions:* 
> 		- *login, logout, social_login, load_model_from_social, password_verify, password_verify_bcrypt, change_password*
> - **libs/Auth.php** - Handles the user's session data
> 	- *functions:* 
> 		- *handleLogin, set_current_user, current_user, checkAccess, handleAccess*
> - **libs/Mail.php** - PHPMailer Wrapper handles sending the mails
> 	- *functions:* 
> 		- *putVars, send, prepare, addImage, emmbedImages, addAttachment, get_html, addLogo, addTo, addCc, addBcc, prepareMessage, isEmail*
> - **libs/Mail/Welcome.php** - Welcome E-Mail, extends Mail.php
> - **libs/Excel.php** - PHPExcel Wrapper handles import of excel files and export customized for Clickdelivery Login App
> 	- *functions:* 
> 		- *getUsers, output, importUsersExcel, importUsersCsv, saveErrors*
>
---
##### Controllers
> - **controllers/login_controller.php** - Handles the user's login, logout, registration page, password changing to view
> 	- *functions:* 
> 		- *index, register, register_ok, login, logout*
> - **controllers/mail_controller.php** - Shows preview versions of Mail (if valid)
> 	- *functions:* 
> 		- *welcome*
> - **controllers/oauth_controller.php** - handles login and registration from Facebook
> 	- *functions:* 
> 		- *callback, login, createUserFromSocial, createSocialData, getProfileImage*
> - **controllers/test_controller.php** - Test Vital parts of the development
> 	- *functions:* 
> 		- *ok, error, warning, test_login, test_users, all_users_test*
> - **controllers/users_controller.php** - handles CRUD requests for Users Table
> 	- *functions:* 
> 		- *ulist, create, read, update, delete, password, register, activation_link*
> - **controllers/files_controller.php** - handles Requests for Excel File
> 	- *functions:* 
> 		- *users, upload_users*
> 	- *note:* it can be tunned for using a background process to process really large files with a standalone script and System::execPhpInBackground, but coudn´t be done in time.
>
---

## Testing
> **controllers/test_controller.php**
> - [Login Test](http://54.187.88.30/loginapp/test/test_login) Tests login and logout functions
> - [All Users Test](http://54.187.88.30/loginapp/test/all_users_test) Tests CRUD capabilities of each type of user
> > 
> 

## Framework
**@laromicas (Lacides Miranda)** based on *[Simple user-authentication solution - MVC FRAMEWORK](https://github.com/panique/php-login/)*

### FrontEnd
#### PHP Framework Files
> - **views/404.php** - 404 Error Page
>
---

> - **.htaccess** - Redirects all petitions to not found elements to index.php
> - **index.php** - Initiates global constraints and autoload functions and executes lib/Bootstrap.php
> - **config.php** - Global constants to configure the site, database and external connections
> - **constants.php** - Global Paths
> - **libs/Bootstrap.php** - The bootloader, examines url and execute the needed controller with the asked parameters
> - **libs/Controller.php** - Functions to extend the controllers
> - **libs/View.php** - Functions to render the frontend
> - **libs/Auth.php** - Some simple funcions to take care of login users and sessions
> - **libs/Cookie.php** - Wrapper to managing cookies
> - **libs/Session.php** - Wrapper to managing sessions
> - **libs/Html.php** - Shortcuts to headers and htmls
> - **libs/Helpers.php** - Helpers and direct functions
> - **controllers/** - All conections of the backend and the frontend are managed with controllers in this directory
> - **controllers/error_controller.php** - takes care of all server error requests
> - **controllers/index_controller.php** - root url
> - **models/** - All models refering to tables in the database
>
---

#### HTML and Javascript Framework
All included inside *assets/* folder
> - [Metronic Template](http://keenthemes.com/preview/metronic/)
> - [jQuery](https://jquery.com/)
> - [AngularJS](https://angularjs.org/)
> - [FontAwesome](http://fontawesome.io/)
>
---

### BackEnd
#### External Libraries
> - **libs/ORM.php** - [idiorm](https://j4mie.github.io/idiormandparis/) class -- a object oriented mapper and query builder *used in conjunction with paris to connect to database*
> - **libs/Model.php** - [paris](https://j4mie.github.io/idiormandparis/) class -- an active record manager - uses idiorm *used to create objects from registers in database*
> - **libs/Facebook/*** - [Facebook SDK for PHP v5](https://github.com/facebook/php-graph-sdk/) class -- allows to access the Facebook Platform from PHP app - *used for facebook integration*
> - **libs/PHPMailer.php** - [PHPMailer](https://github.com/PHPMailer/PHPMailer/) class -- a full-featured email creation and transfer class for PHP *used to send activation link*
> - **libs/PHPExcel.php** - [PHPExcel](https://github.com/PHPOffice/PHPExcel/) class -- a class used to import and export to excel files *to be implemented*
> 