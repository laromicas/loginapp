<?php
/**
 * THIS IS THE CONFIGURATION FILE
 * 
 * For more info about constants please @see http://php.net/manual/en/function.define.php
 * If you want to know why we use "define" instead of "const" @see http://stackoverflow.com/q/2447791/1114320
 */
 //******************* PARAMETROS ****************************
//$emailprueba[0]= "gpltda@nguillers.com.co";

define('EXT_URL', FRNT_URL);
define('EXT_PATH', FRNT_ROOT_PATH);

function define_path($constant, $value) {
	define($constant.'_URL', EXT_URL.$value);
	define($constant.'_PATH', EXT_PATH.$value);
	if(!is_dir(EXT_PATH.$value)) {
		mkdir(EXT_PATH.$value);
	}
}

define('APP_TITLE','Login Test App');

//Configuraciones de rutas
// define_path('MEDIA', 'multimedia/');
// define_path('CONTENTS', 'contenidos/');
define_path('ASSETS', 'assets/');
define_path('CSS', 'assets/css/');
define_path('JS', 'assets/js/');
define_path('ANGULAR', 'angular/');
define_path('USERS', 'files/users/');
define_path('TMP', 'tmp/');
