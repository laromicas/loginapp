<?php
$GLOBALS['status'] = array(
	'A' => 'Active', //Active
	'D' => 'Disabled', //Disabled
	'R' => 'Removed' //Removed
);

$GLOBALS['userTypes'] = array(
	'A' => 'Admin',
	'G' => 'Agent',
	'C' => 'Customer',
	'S' => 'Server',
	'L' => 'Client'
);
