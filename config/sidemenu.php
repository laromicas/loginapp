<?php
$GLOBALS["sidemenu"] = array(
	"My Profile" => array(
		"start" => true,
		"link" => "#!/",
		"icon" => "home",
		"access" => "AGC"
	),
	"User List" => array(
		"start" => false,
		"link" => "#!/users",
		"icon" => "users",
		"access" => "AG"
	),
	// "User Add" => array(
	// 	"start" => false,
	// 	"link" => "#!/user/new",
	// 	"icon" => "user-plus",
	// 	"access" => "A"
	// ),
);